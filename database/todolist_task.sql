-- MySQL dump 10.13  Distrib 8.0.16, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: todolist
-- ------------------------------------------------------
-- Server version	8.0.16

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `task`
--

DROP TABLE IF EXISTS `task`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `task` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `task` json NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `task`
--

LOCK TABLES `task` WRITE;
/*!40000 ALTER TABLE `task` DISABLE KEYS */;
INSERT INTO `task` VALUES (2,'{\"id\": \"2\", \"done\": \"false\", \"name\": \"gotozoo\", \"created_at\": \"2019-07-21 15:54:45.000000\", \"updated_at\": \"2019-07-21 20:08:50.000000\"}'),(5,'{\"id\": \"5\", \"done\": \"false\", \"name\": \"gotozoo\", \"created_at\": \"2019-07-21 18:17:13.000000\", \"updated_at\": \"2019-07-21 19:58:50.000000\"}'),(6,'{\"id\": 4, \"done\": \"false\", \"name\": \"go to zoo\", \"created_at\": \"2019-07-21 19:24:04.000000\", \"updated_at\": \"2019-07-21 19:24:04.000000\"}'),(8,'{\"id\": null, \"done\": \"false\", \"name\": null, \"created_at\": \"2019-07-21 19:38:08.000000\", \"updated_at\": \"2019-07-21 19:38:08.000000\"}'),(9,'{\"id\": null, \"done\": \"false\", \"name\": null, \"created_at\": \"2019-07-21 19:41:17.000000\", \"updated_at\": \"2019-07-21 19:41:17.000000\"}'),(10,'{\"id\": null, \"done\": \"false\", \"name\": \"go to zoo\", \"created_at\": \"2019-07-21 19:44:04.000000\", \"updated_at\": \"2019-07-21 20:34:35.000000\"}'),(11,'{\"id\": 4, \"done\": \"false\", \"name\": \"go to zoo\", \"created_at\": \"2019-07-21 19:48:34.000000\", \"updated_at\": \"2019-07-21 19:48:34.000000\"}'),(12,'{\"id\": null, \"done\": \"false\", \"name\": null, \"created_at\": \"2019-07-21 19:49:21.000000\", \"updated_at\": \"2019-07-21 19:49:21.000000\"}'),(13,'{\"id\": null, \"done\": \"false\", \"name\": null, \"created_at\": \"2019-07-21 19:54:21.000000\", \"updated_at\": \"2019-07-21 19:54:21.000000\"}'),(14,'{\"id\": null, \"done\": \"false\", \"name\": null, \"created_at\": \"2019-07-21 20:00:40.000000\", \"updated_at\": \"2019-07-21 20:00:40.000000\"}'),(15,'{\"id\": null, \"done\": \"false\", \"name\": null, \"created_at\": \"2019-07-21 20:01:38.000000\", \"updated_at\": \"2019-07-21 20:01:38.000000\"}'),(16,'{\"id\": null, \"done\": \"false\", \"name\": null, \"created_at\": \"2019-07-21 20:02:12.000000\", \"updated_at\": \"2019-07-21 20:02:12.000000\"}'),(17,'{\"id\": null, \"done\": \"false\", \"name\": null, \"created_at\": \"2019-07-21 20:07:07.000000\", \"updated_at\": \"2019-07-21 20:07:07.000000\"}'),(18,'{\"id\": null, \"done\": \"false\", \"name\": null, \"created_at\": \"2019-07-21 20:07:36.000000\", \"updated_at\": \"2019-07-21 20:07:36.000000\"}'),(19,'{\"id\": null, \"done\": \"false\", \"name\": null, \"created_at\": \"2019-07-21 20:08:04.000000\", \"updated_at\": \"2019-07-21 20:08:04.000000\"}'),(20,'{\"id\": 1, \"done\": \"false\", \"name\": \"gotozoo\", \"created_at\": \"2019-07-21 20:08:59.000000\", \"updated_at\": \"2019-07-21 20:08:59.000000\"}'),(21,'{\"id\": null, \"done\": \"false\", \"name\": null, \"created_at\": \"2019-07-21 20:26:08.000000\", \"updated_at\": \"2019-07-21 20:26:08.000000\"}'),(22,'{\"id\": null, \"done\": \"false\", \"name\": null, \"created_at\": \"2019-07-21 20:26:33.000000\", \"updated_at\": \"2019-07-21 20:26:33.000000\"}'),(23,'{\"id\": null, \"done\": \"false\", \"name\": null, \"created_at\": \"2019-07-21 20:26:48.000000\", \"updated_at\": \"2019-07-21 20:26:48.000000\"}'),(24,'{\"id\": null, \"done\": \"false\", \"name\": \"doing homework\", \"created_at\": \"2019-07-21 20:28:24.000000\", \"updated_at\": \"2019-07-21 22:13:55.000000\"}'),(25,'{\"id\": null, \"done\": \"false\", \"name\": \"go to park\", \"created_at\": \"2019-07-21 20:28:28.000000\", \"updated_at\": \"2019-07-21 20:35:08.000000\"}'),(26,'{\"id\": 26, \"done\": \"false\", \"name\": \"pergi ke bioskop\", \"created_at\": \"2019-07-21 22:14:30.000000\", \"updated_at\": \"2019-07-21 22:14:30.000000\"}'),(27,'{\"id\": 27, \"done\": \"false\", \"name\": \"go to park\", \"created_at\": \"2019-07-22 10:45:44.000000\", \"updated_at\": \"2019-07-22 10:47:57.000000\"}');
/*!40000 ALTER TABLE `task` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-07-22 10:54:39
