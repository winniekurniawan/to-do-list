const mysql = require('mysql');
const express = require('express');
var app = express();

const bodyparser = require('body-parser');
app.use(bodyparser.json());

var mysqlConnection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'root',
    database: 'todolist',
    multipleStatements: true
});

mysqlConnection.connect((err) => {
    if (!err)
        console.log('DB connection succeded');
    else
        console.log('DB connection failed. \n Error : ' + JSON.stringify(err, undefined, 2));
});

app.listen(3000, () => console.log('Express server is running at port no : 3000'));

//get all to do list
app.get('/todolist', (req, res) => {
    mysqlConnection.query('SELECT * FROM task', (err, rows, fileds) => {
        if (!err)
            // console.log(rows);
            res.send(rows);
        else
            console.log(err);
    })
});

//get an to do list
app.get('/todolist/:id', (req, res) => {
    mysqlConnection.query('SELECT * FROM task WHERE id=?', [req.params.id], (err, rows, fileds) => {
        if (!err)
            // console.log(rows);
            res.send(rows);
        else
            console.log(err);
    })
});

//delete an to do list
app.delete('/todolist/:id', (req, res) => {
    mysqlConnection.query('DELETE FROM task WHERE id=?', [req.params.id], (err, rows, fileds) => {
        if (!err)
            // console.log(rows);
            res.send('Deleted successfully.');
        else
            console.log(err);
    })
});

//insert an to do list
app.post('/todolist', (req, res) => {
    let emp = req.body;
    var sql = 'SET @id=?; SET @name=?; \
        CALL insertUpdateTask(@id, @name);';
    mysqlConnection.query(sql, [emp.id, emp.name], (err, rows, fileds) => {
        if (!err)
            rows.forEach(element => {
                if (element.constructor == Array)
                    res.send('status : ' + element[0].hasil);
            });
        else
            console.log(err);
    })
});

//update an to do list
app.put('/todolist', (req, res) => {
    let emp = req.body;
    var sql = 'SET @id=?; SET @name=?; \
        CALL insertUpdateTask(@id, @name);';
    mysqlConnection.query(sql, [emp.id, emp.name], (err, rows, fileds) => {
        if (!err)
            res.send('Updated successfully');
        else
            console.log(err);
    })
});